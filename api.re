let get_token () cat token;
let api () s+ "https://api.telegram.org/bot" (get_token);
let curl_opt (is_up) ? @is_up | = "#t" => "-F", | _ => "--form-string";

let send_message (text)
	curl -s
		(s+ (api) "/sendMessage")
		(curl_opt #f) (s+ "text=" text)
		(curl_opt #f) (s+ "chat_id=" (chat_id))
		(curl_opt #f) (s+ "reply_to_message_id=" (message_id))
		-o /dev/null;
	

let send_document (file is_up)
	curl -s
		(s+ (api) "/sendDocument")
		(curl_opt is_up) (s+ "document=" file)
		(curl_opt is_up) (s+ "chat_id=" (chat_id))
		(curl_opt is_up) (s+ "reply_to_message_id=" (message_id))
		-o /dev/null;

let send_sticker (sticker_id is_up)
	curl -s
		(s+ (api) "/sendSticker")
		(curl_opt is_up) (s+ "sticker=" sticker_id)
		(curl_opt is_up) (s+ "chat_id=" (chat_id))
		(curl_opt is_up) (s+ "reply_to_message_id=" (message_id))
		-o /dev/null;
