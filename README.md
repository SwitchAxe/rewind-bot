# rewind-bot

rewind-bot is a general purpose telegram bot written in Rewind, example with current code: [rewindbot](https://t.me/sofianeekbot)

## Requirements

- [Rewind](https://github.com/SwitchAxe/Rewind)

```bash
git clone https://github.com/SwitchAxe/Rewind.git && cd Rewind && make
```

## Usage

Respond to incoming updates using [setWebhook](https://github.com/SwitchAxe/Rewind.git) to execute `main.re` with the json-serialized update as an argument:
```bash
./rewind main.re -- "$json"
```

## License

[GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html)
