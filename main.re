let start_time / (toi (date +%s%N)) 1000000;

load utils.re;
load api.re;

let user_text () json "-e message -e text -u";
let reply_text () json "-e message -e reply_to_message -e text -u";
let chat_id () json "-e message -e chat -e id -u";
let message_id () json "-e message -e message_id -u";
let message_date () toi (json "-e message -e date -u");
let sticker () json "-e message -e sticker -e file_id -u";
let sticker_uid () json "-e message -e sticker -e file_unique_id -u";

? user_text
	| in ["a" "A"] => send_message (user_text),
	| in ["s" "S"] => send_message (<<< (tr "[sS]" "[nN]") (user_text)),
	| in ["n" "N"] => send_message (<<< (tr "[nN]" "[sS]") (user_text)),
	| = "O_O" => send_message (user_text);


| = (sticker_uid) "AgADAwAD2s_IHA" => send_sticker (sticker) "#f";

let command_strlist cmd/args (user_text);
let command first $command_strlist;
let command_args rest $command_strlist;

print $command "\n";
print "command_args" $command_args "\n";

let prepare_source ()
    (let zip_file (s+ "source-" (source_hash) ".zip")),
    (zip -r $zip_file main.re api.re utils.re LICENSE README.md),
    (send_document (s+ "@" $zip_file) #t),
    (rm $zip_file);

? @$command
	| = "!fortune" => send_message (fortune),
	| = "!ping" =>
		send_message
			(printf "%s\n" "pong"
				(s+ "api: " (-> (ping -c 1 api.telegram.org) (sed -n "s/.*time=//p")))
				(s+ "response time: " (printf "%s" (- (toi (date +%s)) (message_date))) " s")
				(s+ "script time: " (printf "%s" (- (/ (toi (date +%s%N)) 1000000) $start_time)) " ms")),
	| = "!re" =>
		send_message
			(<<<
				(->
					(firejail
						--whitelist=/usr/local/bin/rewind
						--blacklist=/usr/bin
						--blacklist=/bin
						--read-only=/
						--rlimit-as=100000000 --rlimit-cpu=100 rewind)
					(tail -n +2))
				(ltos $command_args)),
	| = "!sed" =>
		send_message
			(<<<
				(sed --sandbox (ltos $command_args))
				(reply_text)),
	| = "!source" => prepare_source;
