let rest (l) tl (length l) (tl (- (length l) 1) l);

let strsplit_aux (l acc cur) | = l [] => ++ acc cur,
    		    	     | = (first l) " " =>
			       strsplit_aux (rest l) (++ acc cur) "",
			     | else =>
			       strsplit_aux (rest l) acc (s+ cur (first l));

let strsplit (s) strsplit_aux (stol s) [] "";

let cmd/args-aux (l acc) | = l [] => [acc],
    		    	 | = (first l) " " => ++ acc (ltos (rest l)),
			 | else => cmd/args-aux (rest l) (s+ acc (first l));



let cmd/args (s) cmd/args-aux (stol s) "";

let ltoa_aux (l acc) | = l [] => acc,
    	     	     | = (first l) "-e" =>
		       ltoa_aux (rest (rest l)) (++ acc (hd 2 l)),
		     | else => ltoa_aux (rest l) (++ acc (first l));


let ltoa (l) ltoa_aux l [];
let stoa (s) ltoa (strsplit s);

let json_aux (fieldl)
	-> (printf "%s" (cmd 1)) (jshon -Q fieldl);

let json (field_s) json_aux (stoa field_s);

let source_hash ()
	->
		(printf "%s" (chat_id) (update_id))
		(md5sum)
		(cut -f 1 -d " ");
